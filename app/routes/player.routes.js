module.exports = (app) => {
  const players = require("../controllers/player.controller.js");

  var router = require("express").Router();

  // Player Endpoints
  router.get("/", players.index);

  /**
   * @swagger
   * /api/players:
   *   post:
   *     summary: Create a player.
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               username:
   *                 type: string
   *                 description: Player's Username.
   *                 example: Wendy
   *               email:
   *                 type: string
   *                 description: Player's Email.
   *                 example: wendy@email.com
   *               password:
   *                 type: string
   *                 description: Player's Password.
   *                 example: wendy1234
   *               exp:
   *                 type: integer
   *                 description: Player's Experience.
   *                 example: 2000
   *     responses:
   *       201:
   *         description: Create Player
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 data:
   *                   type: object
   *                   properties:
   *                       username:
   *                         type: string
   *                         description: Player's Name.
   *                         example: ryujin
   *                       email:
   *                         type: string
   *                         description: Player's Email.
   *                         example: ryujin@email.com
   *                       password:
   *                         type: string
   *                         description: Player's Password.
   *                         example: admin1234
   *                       experience:
   *                         type: integer
   *                         description: Player's Experience.
   *                         example: 100
   *                       lvl:
   *                         type: integer
   *                         description: Player's Level.
   *                         example: 5
   */
  router.post("/players", players.create);
  /**
   * @swagger
   * /api/players:
   *   get:
   *     summary: Retrieve a list of Binar Challange players.
   *     description: Retrieve a list of players from Binar Challange.Can be used to populate a list of fake users when prototyping or testing an API.
   *     responses:
   *       200:
   *         description: A JSON array of players.
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 data:
   *                   type: array
   *                   items:
   *                     type: object
   *                     properties:
   *                       id:
   *                         type: integer
   *                         description: Player's ID.
   *                         example: 1
   *                       username:
   *                         type: string
   *                         description: Player's Name.
   *                         example: ryujin
   *                       email:
   *                         type: string
   *                         description: Player's Email.
   *                         example: ryujin@email.com
   *                       password:
   *                         type: string
   *                         description: Player's Password.
   *                         example: admin1234
   *                       experience:
   *                         type: integer
   *                         description: Player's Experience.
   *                         example: 100
   *                       lvl:
   *                         type: integer
   *                         description: Player's Level.
   *                         example: 5
   */
  router.get("/players", players.findAll);

  /**
   * @swagger
   * /api/players/{id}:
   *   get:
   *     summary: Retrieve a single Binar Challange players.
   *     description: Retrieve a single player from Binar Challange.
   *     parameters:
   *       - in: path
   *         name: id
   *         required: true
   *         description: Numeric ID of the player to retrieve.
   *         schema:
   *           type: integer
   *     responses:
   *       200:
   *         description: A single player.
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                       id:
   *                         type: integer
   *                         description: Player's ID.
   *                         example: 1
   *                       username:
   *                         type: string
   *                         description: Player's Name.
   *                         example: ryujin
   *                       email:
   *                         type: string
   *                         description: Player's Email.
   *                         example: ryujin@email.com
   *                       password:
   *                         type: string
   *                         description: Player's Password.
   *                         example: admin1234
   *                       experience:
   *                         type: integer
   *                         description: Player's Experience.
   *                         example: 100
   *                       lvl:
   *                         type: integer
   *                         description: Player's Level.
   *                         example: 5
   */
  router.get("/players/:id", players.findById);

  /**
   * @swagger
   * /api/players/{id}:
   *   put:
   *     summary: Update a player.
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               username:
   *                 type: string
   *                 description: Player's Username.
   *                 example: Wendy
   *               email:
   *                 type: string
   *                 description: Player's Email.
   *                 example: wendy@email.com
   *               password:
   *                 type: string
   *                 description: Player's Password.
   *                 example: wendy1234
   *               exp:
   *                 type: integer
   *                 description: Player's Experience.
   *                 example: 2000
   *     parameters:
   *       - in: path
   *         name: id
   *         required: true
   *         description: Numeric ID of the player to retrieve.
   *         schema:
   *           type: integer
   *     responses:
   *       200:
   *         description: Update Player
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 data:
   *                   type: object
   *                   properties:
   *                       username:
   *                         type: string
   *                         description: Player's Name.
   *                         example: ryujin
   *                       email:
   *                         type: string
   *                         description: Player's Email.
   *                         example: ryujin@email.com
   *                       password:
   *                         type: string
   *                         description: Player's Password.
   *                         example: admin1234
   *                       experience:
   *                         type: integer
   *                         description: Player's Experience.
   *                         example: 100
   *                       lvl:
   *                         type: integer
   *                         description: Player's Level.
   *                         example: 5
   */
  router.put("/players/:id", players.update);

  /**
   * @swagger
   * /api/players/exp/{id}:
   *   post:
   *     summary: Add player's experience.
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               exp:
   *                 type: integer
   *                 description: Player's Experience.
   *                 example: 5000
   *     parameters:
   *       - in: path
   *         name: id
   *         required: true
   *         description: Numeric ID of the player to retrieve.
   *         schema:
   *           type: integer
   *     responses:
   *       200:
   *         description: Add player's experience
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 data:
   *                   type: object
   *                   properties:
   *                       username:
   *                         type: string
   *                         description: Player's Name.
   *                         example: ryujin
   *                       email:
   *                         type: string
   *                         description: Player's Email.
   *                         example: ryujin@email.com
   *                       experience:
   *                         type: integer
   *                         description: Player's Experience.
   *                         example: 5000
   *                       lvl:
   *                         type: integer
   *                         description: Player's Level.
   *                         example: 5
   */
  router.post("/players/exp/:id", players.getExperience);
  /**
   * @swagger
   * /api/players/{id}:
   *   delete:
   *     summary: Delete a player.
   *     parameters:
   *       - in: path
   *         name: id
   *         required: true
   *         description: Numeric ID of the player to retrieve.
   *         schema:
   *           type: integer
   *     responses:
   *       200:
   *         description: Delete Player
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 data:
   *                   type: object
   */
  router.delete("/players/:id", players.delete);

  // API prefix
  app.use("/api", router);
};
